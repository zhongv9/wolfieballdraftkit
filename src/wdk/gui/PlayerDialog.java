package wdk.gui;

import wdk.data.Player;
import static wdk.gui.WDK_GUI.CLASS_HEADING_LABEL;
import static wdk.gui.WDK_GUI.CLASS_PROMPT_LABEL;
import static wdk.gui.WDK_GUI.PRIMARY_STYLE_SHEET;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wdk.data.FantasyTeam;
import static wdk.gui.FantasyDialog.ADD_FANTASY_TEAM_TITLE;

/**
 *
 * @author McKillaGorilla
 */
public class PlayerDialog extends Stage {
    // THIS IS THE OBJECT DATA BEHIND THIS UI
    Player player;
    FantasyTeam fantasyTeam;
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label fullNameLabel;
    Label firstNameLabel;
    Label positionLabel;
    Label positionsLabel;
    Label fantasyTeamsLabel;
    Label contractLabel;
    Label salaryLabel;
    TextField salaryTextField;
    TextField firstNameTextField;
    Label lastNameLabel;
    TextField lastNameTextField;
    Label proTeamsLabel;
    ComboBox proTeamsComboBox;
    ComboBox fantasyTeamsComboBox;
    ComboBox positionComboBox;
    ComboBox contractComboBox;
    CheckBox a, b, c, d, e, f, g;
    Button completeButton;
    Button cancelButton;
     
    // THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection;
    
    // CONSTANTS FOR OUR UI
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String FIRST_NAME_PROMPT = "First Name: ";
    public static final String LAST_NAME_PROMPT = "Last Name: ";
    public static final String PRO_TEAM_PROMPT = "Pro Team: ";
    public static final String PLAYER_HEADING = "Player Details";
    public static final String ADD_PLAYER_TITLE = "Add New Player";
    public static final String EDIT_PLAYER_TITLE = "Edit Player";
    
    Label fantasyTeamLabel;
    Label fantasyOwnerLabel;
    TextField fantasyTeamTextField;
    TextField fantasyOwnerTextField;
    public static final String OWNER_PROMPT = "Owner: ";
    public static final String FANTASY_HEADING = "Fantasy Team Details";
    public static final String ADD_FANTASY_TEAM_TITLE = "Add New Fantasy Team";
    public static final String EDIT_FANTASY_TEAM_TITLE = "Edit Fantasy Team";
    
    ObservableList<String> teams;
    /**
     * Initializes this dialog so that it can be used for either adding
     * new schedule items or editing existing ones.
     * 
     * @param primaryStage The owner of this modal dialog.
     */
    public PlayerDialog(Stage primaryStage) {
        // FIRST MAKE OUR PLAYER AND INITIALIZE
        // IT WITH DEFAULT VALUES
        
       
        player = new Player();
        
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(PLAYER_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        // NOW THE FIRST_NAME 
        firstNameLabel = new Label(FIRST_NAME_PROMPT);
        firstNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        firstNameTextField = new TextField();
        firstNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setFirstName(newValue);
        });
        
        // NOW THE LAST_NAME 
        lastNameLabel = new Label(LAST_NAME_PROMPT);
        lastNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        lastNameTextField = new TextField();
        lastNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setLastName(newValue);
        });
        
        // AND THE NUMBER OF PRO_TEAM
        proTeamsLabel = new Label(PRO_TEAM_PROMPT);
        proTeamsLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        proTeamsComboBox = new ComboBox();
        proTeamsComboBox.getItems().addAll("ATL", "AZ", "CHC", "CIN", "COL", "LAD", "MIA", "MIL", "NYM", "PHI", "PIT", "SD", "SF", "STL", "TOR", "WSH");
        proTeamsComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                player.setProTeam(newValue.toString());
            }
            
        });
        
        a = new CheckBox("C");
        a.setOnAction(e -> { player.setPositions("C");});
        b = new CheckBox("1B");
        b.setOnAction(e -> { player.setPositions("1B");});
        c = new CheckBox("3B");
        c.setOnAction(e -> { player.setPositions("3B");});
        d = new CheckBox("2B");
        d.setOnAction(e -> { player.setPositions("2B");});
        e = new CheckBox("SS");
        e.setOnAction(e -> { player.setPositions("SS");});
        f = new CheckBox("OF");
        f.setOnAction(e -> { player.setPositions("OF");});
        g = new CheckBox("P");
        g.setOnAction(e -> { player.setPositions("P");});
        
        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            PlayerDialog.this.selection = sourceButton.getText();
            PlayerDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel,      0, 0, 2, 1);
        gridPane.add(firstNameLabel,        0, 1, 1, 1);
        gridPane.add(firstNameTextField,    1, 1, 1, 1);
        gridPane.add(lastNameLabel,        0, 2, 1, 1);
        gridPane.add(lastNameTextField,    1, 2, 1, 1);
        gridPane.add(proTeamsLabel,     0, 3, 1, 1);
        gridPane.add(proTeamsComboBox,  1, 3, 1, 1);
        gridPane.add(a,                 0, 4, 1, 1);
        gridPane.add(b,                 1, 4, 1, 1);
        gridPane.add(c,                 2, 4, 1, 1);
        gridPane.add(d,                 3, 4, 1, 1);
        gridPane.add(e,                 4, 4, 1, 1);
        gridPane.add(f,                 5, 4, 1, 1);
        gridPane.add(g,                 6, 4, 1, 1);
        gridPane.add(completeButton,    0, 6, 1, 1);
        gridPane.add(cancelButton,      1, 6, 1, 1);

        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    /*******************************************************************************************************************************************************************************/
    
        public PlayerDialog(Stage primaryStage, String nothing) {
        // FIRST MAKE OUR PLAYER AND INITIALIZE
        // IT WITH DEFAULT VALUES
        player = new Player();
        teams = FXCollections.observableArrayList();
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(PLAYER_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        // NOW THE FIRST_NAME 
        fullNameLabel = new Label();
        fullNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        
        positionsLabel = new Label();
        //positionsLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        
        // AND THE NUMBER OF PRO_TEAM
        fantasyTeamsLabel = new Label("Fantasy Team:");
        //fantasyTeamsLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        fantasyTeamsComboBox = new ComboBox();
        fantasyTeamsComboBox.getItems().addAll("");
        fantasyTeamsComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                player.setFantasyTeam(newValue.toString());
            }
        });
        
        positionLabel = new Label("Position:");
        //positionLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        positionComboBox = new ComboBox();
        positionComboBox.getItems().addAll("");
        positionComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                player.setPosition(newValue.toString());
            }
        });
        
        contractLabel = new Label("Contract:");
        //contractLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        contractComboBox = new ComboBox();
        contractComboBox.getItems().addAll("S1", "S2", "X");
        contractComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                player.setContract(newValue.toString());
            }
        });
        
        salaryLabel = new Label("Salary ($):");
        salaryLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        salaryTextField = new TextField();
        salaryTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setSalary(newValue);
        });
        
        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            PlayerDialog.this.selection = sourceButton.getText();
            PlayerDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        
//        ImageView img = new ImageView();
//        img.setFitHeight(100);
//        img.setFitWidth(200);
//        Image photo = new Image("AAA_PhotoMissing.jpg");
//        img.setImage(photo);
        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel,      0, 0, 2, 1);
        gridPane.add(fullNameLabel,     0, 1, 1, 1);
        gridPane.add(positionsLabel,    0, 2, 1, 1);
        gridPane.add(fantasyTeamsLabel, 0, 3, 1, 1);
        gridPane.add(fantasyTeamsComboBox, 1, 3, 1, 1);
        gridPane.add(positionLabel,     0, 4, 1, 1);
        gridPane.add(positionComboBox,  1, 4, 1, 1);
        gridPane.add(contractLabel,     0, 5, 1, 1);
        gridPane.add(contractComboBox,  1, 5, 1, 1);
        gridPane.add(salaryLabel,       0, 6, 1, 1);
        gridPane.add(salaryTextField,   1, 6, 1, 1);
        gridPane.add(completeButton,    0, 7, 1, 1);
        gridPane.add(cancelButton,      1, 7, 1, 1);
        //gridPane.add(img, 0, 2, 1, 1);
        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
/***********************************************************************************************************************************************************************************/
        public PlayerDialog(Stage primaryStage, String nothing, String nothings) {
        // FIRST MAKE OUR PLAYER AND INITIALIZE
        // IT WITH DEFAULT VALUES
        player = new Player();
        
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(FANTASY_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        fantasyTeamLabel = new Label("Name:");
        fantasyTeamLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        fantasyOwnerLabel = new Label("Owner:");
        fantasyOwnerLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        fantasyTeamTextField = new TextField();
        fantasyTeamTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            fantasyTeam.setTeamName(newValue);
        });
        fantasyOwnerTextField = new TextField();
        fantasyOwnerTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            fantasyTeam.setFantasyOwner(newValue);
        });
        
        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            PlayerDialog.this.selection = sourceButton.getText();
            PlayerDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        
        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel,      0, 0, 2, 1);
        gridPane.add(fantasyTeamLabel,     0, 1, 1, 1);
        gridPane.add(fantasyTeamTextField,     1, 1, 1, 1);
        gridPane.add(fantasyOwnerLabel,     0, 2, 1, 1);
        gridPane.add(fantasyOwnerTextField,     1, 2, 1, 1);
        gridPane.add(completeButton,    0, 4, 1, 1);
        gridPane.add(cancelButton,      1, 4, 1, 1);
        //gridPane.add(img, 0, 2, 1, 1);
        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public Player getPlayer() { 
        return player;
    }
    
    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     * 
     * @param message Message to appear inside the dialog.
     */
    public Player showAddPlayerDialog() {
        // SET THE DIALOG TITLE
        setTitle(ADD_PLAYER_TITLE);
        
        // RESET THE SCHEDULE ITEM OBJECT WITH DEFAULT VALUES
        player = new Player();
        
        // LOAD THE UI STUFF
        firstNameTextField.setText(player.getFirstName());
        lastNameTextField.setText(player.getLastName());
        //proTeamsComboBox.getSelectionModel().select(0);
        
        // AND OPEN IT UP
        this.showAndWait();
        
        return player;
    }
    
    public void loadGUIData() {
        // LOAD THE UI STUFF
//        firstNameTextField.setText(player.getFirstName());
//        lastNameTextField.setText(player.getLastName());
        //int proTeamsIndex = player.getProTeam()-1;
        //proTeamsComboBox.getSelectionModel().select(positionsIndex);
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    public void showEditPlayerDialog(Player playerToEdit) {
        // SET THE DIALOG TITLE
        setTitle(EDIT_PLAYER_TITLE);
        
        // LOAD THE PLAYER INTO OUR LOCAL OBJECT
//        String imageFile = new String(playerToEdit.getFirstName() + "" + playerToEdit.getLastName() + ".jpg");
//        ImageView us = new ImageView(new Image(imageFile));
        fullNameLabel.setText(playerToEdit.getFirstName() + " " + playerToEdit.getLastName());
        
        fantasyTeamsComboBox = new ComboBox(teams);
        String posString = playerToEdit.getPositions();
        String[] positions = posString.split("_");
        positionComboBox.getItems().clear();
        for(int i = 0; i < positions.length; i++){
            positionComboBox.getItems().add(positions[i]);
        }
        
        
        //playerToEdit.setFantasyTeam(fantasyTeamsComboBox.getSelectionModel().getSelectedItem().toString());
//        playerToEdit.setPosition(positionComboBox.getSelectionModel().getSelectedItem().toString());
//        playerToEdit.setContract(contractComboBox.getSelectionModel().getSelectedItem().toString());
//        playerToEdit.setSalary(salaryTextField.getText());
        // AND THEN INTO OUR GUI
        loadGUIData();
               
        // AND OPEN IT UP
        this.showAndWait();
    }
    
    public FantasyTeam getFantasyTeam() { 
        return fantasyTeam;
    }
    
    public FantasyTeam showAddFantasyTeamDialog() {
        // SET THE DIALOG TITLE
        setTitle(ADD_FANTASY_TEAM_TITLE);
        
        // RESET THE SCHEDULE ITEM OBJECT WITH DEFAULT VALUES
        fantasyTeam = new FantasyTeam();
        
        // LOAD THE UI STUFF
        fantasyTeamTextField.setText(fantasyTeam.getTeamName());
        fantasyOwnerTextField.setText(fantasyTeam.getFantasyOwner());
        //proTeamsComboBox.getSelectionModel().select(0);
        
        // AND OPEN IT UP
        this.showAndWait();
        
        return fantasyTeam;
    }
    
    public void addFantasyTeamStringName(String teamName) {
        teams.add(teamName);
    }
    
    public void removeFantasyTeamStringName(String teamToRemove){
        teams.remove(teamToRemove);
    }
    
    public ObservableList<String> getFantasyTeamStringNames(){
        return teams;
    }
}