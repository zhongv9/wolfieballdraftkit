package wdk.gui;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.FileChooser;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import static wdk.WDK_StartupConstants.*;
import wdk.WDK_PropertyType;
import wdk.controller.CourseEditController;
import wdk.data.Course;
import wdk.data.CourseDataManager;
import wdk.data.CourseDataView;
import wdk.data.CoursePage;
import wdk.controller.FileController;
import wdk.data.Instructor;
import wdk.data.Semester;
import wdk.data.Subject;
import wdk.file.CourseFileManager;
import wdk.file.CourseSiteExporter;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wdk.controller.PlayerController;
import wdk.data.Player;

/**
 * This class provides the Graphical User Interface for this application,
 * managing all the UI components for editing a Course and exporting it to a
 * site.
 *
 * @author Richard McKenna
 */
public class WDK_GUI implements CourseDataView {

    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS GUI'S COMPONENTS TO A STYLE SHEET THAT IT USES

    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "csb_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_SUBJECT_PANE = "subject_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String EMPTY_TEXT = "";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;

    // THIS MANAGES ALL OF THE APPLICATION'S DATA
    CourseDataManager dataManager;

    // THIS MANAGES COURSE FILE I/O
    CourseFileManager courseFileManager;

    // THIS MANAGES EXPORTING OUR SITE PAGES
    CourseSiteExporter siteExporter;

    // THIS HANDLES INTERACTIONS WITH FILE-RELATED CONTROLS
    FileController fileController;

    // THIS HANDLES INTERACTIONS WITH COURSE INFO CONTROLS
    CourseEditController courseController;
    
    PlayerController playerController;
    
    // THIS IS THE APPLICATION WINDOW
    Stage primaryStage;

    // THIS IS THE STAGE'S SCENE GRAPH
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane wdkPane;
    
    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newCourseButton;
    Button saveCourseButton;
    Button exportSiteButton;
    Button exitButton;

    // WE'LL ORGANIZE OUR WORKSPACE COMPONENTS USING A BORDER PANE
    BorderPane workspacePane;
    boolean workspaceActivated;

    // WE'LL PUT THIS IN THE TOP OF THE WORKSPACE, IT WILL
    // HOLD TWO OTHER PANES FULL OF CONTROLS AS WELL AS A LABEL
    VBox playersPane;
    Label playerPaneLabel;
    GridPane playerGridPane;
    
    // THESE ARE THE CONTROLS FOR THE BASIC SCHEDULE PAGE HEADER INFO
    GridPane courseInfoPane;
    Label courseInfoLabel;
    Label courseSubjectLabel;
    ComboBox courseSubjectComboBox;
    Label courseNumberLabel;
    TextField courseNumberTextField;
    Label courseTitleLabel;
    TextField courseTitleTextField;
    Label instructorNameLabel;
    TextField instructorNameTextField;
    Label instructorURLLabel;
    TextField instructorURLTextField;

    // SCHEDULE CONTROLS
    VBox schedulePane;
    VBox scheduleInfoPane;
    Label scheduleInfoHeadingLabel;
    SplitPane splitScheduleInfoPane;

    // THIS REGION IS FOR MANAGING SCHEDULE ITEMS OTHER THAN LECTURES AND HWS
    VBox playerControlsBox;
    HBox playerControlsToolbar;
    Button addPlayerButton;
    Button removePlayerButton;
    Label searchPlayerLabel;
    TextField searchPlayerTextfield;
    TableView<Player> playersTable;
    VBox playersTableBox;
    TableColumn firstNameColumn;
    TableColumn lastNameColumn;
    TableColumn proTeamColumn;
    TableColumn positionsColumn;
    TableColumn yearOfBirthColumn;
    TableColumn runWinsColumn;
    TableColumn homeRunsSavesColumn;
    TableColumn rbiStrikeoutColumn;
    TableColumn sbERAColumn;
    TableColumn baWHIPColumn;
    TableColumn notesColumn;
    private final ObservableList<Player> players = FXCollections.observableArrayList();
    

    static final String COL_LASTNAME = "Last";
    static final String COL_FIRSTNAME = "First";
    static final String COL_PROTEAM = "Pro Team";
    static final String COL_POSITION = "Position";
    static final String COL_POSITIONS = "Positions";
    static final String COL_BIRTH = "Date of Birth";
    static final String COL_RUNWINS = "R/W";
    static final String COL_HOMERUNS = "HR/SV";
    static final String COL_STRIKEOUTS = "RBI/K";
    static final String COL_SBERA = "SB/ERA";
    static final String COL_BAWHIP = "BA/WHIP";
    static final String COL_VALUE = "Estimated Value";
    static final String COL_NOTES = "Notes";
    static final String COL_SALARY = "Salary";
    
    // THIS REGION IS FOR RADIOBUTTONS AND RELATED BOXES
    GridPane radioButtonsPane;
    HBox radioButtonsToolbar;
    RadioButton a, b, c, d, e, f, g, h, i, j, k;
    
    // THIS IS THE FOR BOTTOM BASEBALL CONTROLS
    FlowPane baseballToolbarPane;
    Button fantasyTeamsButton;
    Button playersButton;
    Button standingsButton;
    Button draftButton;
    Button proTeamsButton;
    
    // VBOXES FOR DIFFERENT PANES
    GridPane fantasyGridPane;
    VBox fantasyTeamControlsBox;
    HBox fantasyTeamControlsToolbar;
    Button addFantasyTeamButton;
    Button removeFantasyTeamButton;
    Label draftNameLabel;
    TextField draftNameTextfield;
    VBox fantasyPane;
    Label fantasyPaneLabel;
    Label selectTeamLabel;
    ComboBox fantasyTeamComboBox;
    VBox standingsPane;
    Label standingsLabel;
    VBox draftPane;
    Label draftLabel;
    VBox proTeamsPane;
    Label proTeamsLabel;
    TableView fantasyTeamsTable;
    
    TableColumn firstNameFColumn;
    TableColumn lastNameFColumn;
    TableColumn proTeamFColumn;
    TableColumn positionFColumn;
    TableColumn positionsFColumn;
    TableColumn yearOfBirthFColumn;
    TableColumn runWinsFColumn;
    TableColumn homeRunsSavesFColumn;
    TableColumn rbiStrikeoutFColumn;
    TableColumn sbERAFColumn;
    TableColumn baWHIPFColumn;
    TableColumn valueFColumn;
    TableColumn salaryFColumn;
        
    final String JSON_HITTERS = "Hitters";
    final String JSON_TEAM = "TEAM";
    final String JSON_LAST_NAME = "LAST_NAME";
    final String JSON_FIRST_NAME = "FIRST_NAME";
    final String JSON_POSITION = "QP";
    final String JSON_AB = "AB";
    final String JSON_R = "R";
    final String JSON_H = "H";
    final String JSON_HR = "HR";
    final String JSON_RBI = "RBI";
    final String JSON_SB = "SB";
    final String JSON_NOTES = "NOTES";
    final String JSON_YEAR_OF_BIRTH = "YEAR_OF_BIRTH";
    final String JSON_NATION_OF_BIRTH = "NATION_OF_BIRTH";
    
    final String JSON_PITCHERS = "Pitchers";
    final String JSON_IP = "IP";
    final String JSON_ER = "ER";
    final String JSON_W = "W";
    final String JSON_SV = "SV";
    final String JSON_BB = "BB";
    final String JSON_K = "K";
    
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    /**
     * Constructor for making this GUI, note that it does not initialize the UI
     * controls. To do that, call initGUI.
     *
     * @param initPrimaryStage Window inside which the GUI will be displayed.
     */
    public WDK_GUI(Stage initPrimaryStage) {
        primaryStage = initPrimaryStage;
    }

    /**
     * Accessor method for the data manager.
     *
     * @return The CourseDataManager used by this UI.
     */
    public CourseDataManager getDataManager() {
        return dataManager;
    }

    /**
     * Accessor method for the file controller.
     *
     * @return The FileController used by this UI.
     */
    public FileController getFileController() {
        return fileController;
    }

    /**
     * Accessor method for the course file manager.
     *
     * @return The CourseFileManager used by this UI.
     */
    public CourseFileManager getCourseFileManager() {
        return courseFileManager;
    }

    /**
     * Accessor method for the site exporter.
     *
     * @return The CourseSiteExporter used by this UI.
     */
    public CourseSiteExporter getSiteExporter() {
        return siteExporter;
    }

    /**
     * Accessor method for the window (i.e. stage).
     *
     * @return The window (i.e. Stage) used by this UI.
     */
    public Stage getWindow() {
        return primaryStage;
    }

    /**
     * Mutator method for the data manager.
     *
     * @param initDataManager The CourseDataManager to be used by this UI.
     */
    public void setDataManager(CourseDataManager initDataManager) {
        dataManager = initDataManager;
    }

    /**
     * Mutator method for the course file manager.
     *
     * @param initCourseFileManager The CourseFileManager to be used by this UI.
     */
    public void setCourseFileManager(CourseFileManager initCourseFileManager) {
        courseFileManager = initCourseFileManager;
    }

    /**
     * Mutator method for the site exporter.
     *
     * @param initSiteExporter The CourseSiteExporter to be used by this UI.
     */
    public void setSiteExporter(CourseSiteExporter initSiteExporter) {
        siteExporter = initSiteExporter;
    }

    /**
     * This method fully initializes the user interface for use.
     *
     * @param windowTitle The text to appear in the UI window's title bar.
     * @param subjects The list of subjects to choose from.
     * @throws IOException Thrown if any initialization files fail to load.
     */
    public void initGUI(String windowTitle) throws IOException {
        // INIT THE TOOLBAR
        initFileToolbar();
        
        initBaseballToolbar();
                
        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
        // TO THE WINDOW YET
        initWorkspace();

        // NOW SETUP THE EVENT HANDLERS
        initEventHandlers();

        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        initWindow(windowTitle);
    }

    /**
     * When called this function puts the workspace into the window,
     * revealing the controls for editing a Course.
     */
    public void activateWorkspace() {
        if (!workspaceActivated) {
            // PUT THE WORKSPACE IN THE GUI
            wdkPane.setCenter(workspacePane);
            workspaceActivated = true;
        }
    }
    
    /**
     * This function takes all of the data out of the courseToReload 
     * argument and loads its values into the user interface controls.
     * 
     * @param courseToReload The Course whose data we'll load into the GUI.
     */
    @Override
    public void reloadCourse(Course courseToReload) {
        // FIRST ACTIVATE THE WORKSPACE IF NECESSARY
        if (!workspaceActivated) {
            activateWorkspace();
        }

        // WE DON'T WANT TO RESPOND TO EVENTS FORCED BY
        // OUR INITIALIZATION SELECTIONS
        courseController.enable(false);

        // FIRST LOAD ALL THE BASIC COURSE INFO
        courseSubjectComboBox.setValue(courseToReload.getSubject());
        courseNumberTextField.setText("" + courseToReload.getNumber());
        courseTitleTextField.setText(courseToReload.getTitle());
        instructorNameTextField.setText(courseToReload.getInstructor().getName());
        instructorURLTextField.setText(courseToReload.getInstructor().getHomepageURL());
        // NOW WE DO WANT TO RESPOND WHEN THE USER INTERACTS WITH OUR CONTROLS
        courseController.enable(true);
    }

    /**
     * This method is used to activate/deactivate toolbar buttons when
     * they can and cannot be used so as to provide foolproof design.
     * 
     * @param saved Describes whether the loaded Course has been saved or not.
     */
    public void updateToolbarControls(boolean saved) {
        // THIS TOGGLES WITH WHETHER THE CURRENT COURSE
        // HAS BEEN SAVED OR NOT
        saveCourseButton.setDisable(saved);

        // ALL THE OTHER BUTTONS ARE ALWAYS ENABLED
        // ONCE EDITING THAT FIRST COURSE BEGINS
        exportSiteButton.setDisable(false);

        // NOTE THAT THE NEW, LOAD, AND EXIT BUTTONS
        // ARE NEVER DISABLED SO WE NEVER HAVE TO TOUCH THEM
    }

    /**
     * This function loads all the values currently in the user interface
     * into the course argument.
     * 
     * @param course The course to be updated using the data from the UI controls.
     */
    public void updateCourseInfo(Course course) {

    }

    /****************************************************************************/
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR GUI */
    /****************************************************************************/
    
    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
        fileToolbarPane = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        newCourseButton = initChildButton(fileToolbarPane, WDK_PropertyType.NEW_COURSE_ICON, WDK_PropertyType.NEW_COURSE_TOOLTIP, false);
        saveCourseButton = initChildButton(fileToolbarPane, WDK_PropertyType.SAVE_COURSE_ICON, WDK_PropertyType.SAVE_COURSE_TOOLTIP, true);
        exportSiteButton = initChildButton(fileToolbarPane, WDK_PropertyType.EXPORT_PAGE_ICON, WDK_PropertyType.EXPORT_PAGE_TOOLTIP, true);
        exitButton = initChildButton(fileToolbarPane, WDK_PropertyType.EXIT_ICON, WDK_PropertyType.EXIT_TOOLTIP, false);
    }
    
    
    private void initBaseballToolbar() {
        baseballToolbarPane = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        fantasyTeamsButton = initChildButton(baseballToolbarPane, WDK_PropertyType.FANTASY_TEAMS_ICON, WDK_PropertyType.FANTASY_TEAMS_TOOLTIP, false);
        playersButton = initChildButton(baseballToolbarPane, WDK_PropertyType.PLAYERS_ICON, WDK_PropertyType.PLAYERS_TOOLTIP, false);
        standingsButton = initChildButton(baseballToolbarPane, WDK_PropertyType.STANDINGS_ICON, WDK_PropertyType.STANDINGS_TOOLTIP, false);
        draftButton = initChildButton(baseballToolbarPane, WDK_PropertyType.DRAFT_ICON, WDK_PropertyType.DRAFT_TOOLTIP, false);
        proTeamsButton = initChildButton(baseballToolbarPane, WDK_PropertyType.PROTEAMS_ICON, WDK_PropertyType.PROTEAMS_TOOLTIP, false);
    }
    // CREATES AND SETS UP ALL THE CONTROLS TO GO IN THE APP WORKSPACE
    private void initWorkspace() throws IOException {
        // THE WORKSPACE HAS A FEW REGIONS, THIS 
        // IS FOR BASIC COURSE EDITING CONTROLS
        initBasicCourseInfoControls();

        // THIS IS FOR SELECTING PAGE LINKS TO INCLUDE
        initPageSelectionControls();

        // THE TOP WORKSPACE HOLDS BOTH THE BASIC COURSE INFO
        // CONTROLS AS WELL AS THE PAGE SELECTION CONTROLS
        initTopWorkspace();

        // THIS IS FOR MANAGING SCHEDULE EDITING
        initScheduleItemsControls();

        // THIS HOLDS ALL OUR WORKSPACE COMPONENTS, SO NOW WE MUST
        // ADD THE COMPONENTS WE'VE JUST INITIALIZED
        workspacePane = new BorderPane();
        workspacePane.setCenter(playersPane);
        //workspacePane.setCenter(playersPane);
        workspacePane.getStyleClass().add(CLASS_BORDERED_PANE);
        workspacePane.setBottom(baseballToolbarPane);
        // NOTE THAT WE HAVE NOT PUT THE WORKSPACE INTO THE WINDOW,
        // THAT WILL BE DONE WHEN THE USER EITHER CREATES A NEW
        // COURSE OR LOADS AN EXISTING ONE FOR EDITING
        workspaceActivated = false;
    }
    
    // INITIALIZES THE TOP PORTION OF THE WORKWPACE UI
    private void initTopWorkspace() throws IOException {
        // HERE'S THE SPLIT PANE, ADD THE TWO GROUPS OF CONTROLS
        playerGridPane = new GridPane();
        
        // THE TOP WORKSPACE PANE WILL ONLY DIRECTLY HOLD 2 THINGS, A LABEL
        // AND A SPLIT PANE, WHICH WILL HOLD 2 ADDITIONAL GROUPS OF CONTROLS
        playersPane = new VBox();
        playerGridPane.getStyleClass().add(CLASS_BORDERED_PANE);

        // HERE'S THE LABEL
        playerPaneLabel = initGridLabel(playerGridPane, WDK_PropertyType.PLAYERS_HEADING_LABEL, CLASS_HEADING_LABEL, 0, 0, 1, 1);
        
        // PLAYER CONTROL BOX WITH ADD, REMOVE, AND SEARCH
        playerControlsBox = new VBox();
        playerControlsToolbar = new HBox();
        addPlayerButton = initChildButton(playerControlsToolbar, WDK_PropertyType.ADD_ICON, WDK_PropertyType.ADD_ITEM_TOOLTIP, false);
        removePlayerButton = initChildButton(playerControlsToolbar, WDK_PropertyType.MINUS_ICON, WDK_PropertyType.REMOVE_ITEM_TOOLTIP, false);
        searchPlayerLabel = initChildLabel(playerControlsToolbar, WDK_PropertyType.SEARCH_PLAYERS_LABEL, CLASS_SUBHEADING_LABEL);
        searchPlayerTextfield = initGridTextField(playerGridPane, LARGE_TEXT_FIELD_LENGTH, EMPTY_TEXT, true, 2, 1, 1, 1);
        playerControlsBox.getStyleClass().add(CLASS_BORDERED_PANE);
        
        // RADIOBUTTONS
        radioButtonsPane = new GridPane();
        radioButtonsToolbar = new HBox();
        a = initRadioButton(radioButtonsPane, "All  ", 0, 3 , 1, 1);
        b = initRadioButton(radioButtonsPane, "C    ", 1, 3 , 1, 1);
        c = initRadioButton(radioButtonsPane, "1B   ", 2, 3 , 1, 1);
        d = initRadioButton(radioButtonsPane, "Cl   ", 3, 3 , 1, 1);
        e = initRadioButton(radioButtonsPane, "3B   ", 4, 3 , 1, 1);
        f = initRadioButton(radioButtonsPane, "2B   ", 5, 3 , 1, 1);
        g = initRadioButton(radioButtonsPane, "MI   ", 6, 3 , 1, 1);
        h = initRadioButton(radioButtonsPane, "SS   ", 7, 3 , 1, 1);
        i = initRadioButton(radioButtonsPane, "OF   ", 8, 3 , 1, 1);
        j = initRadioButton(radioButtonsPane, "U    ", 9, 3 , 1, 1);
        k = initRadioButton(radioButtonsPane, "P   ", 10, 3 , 1, 1);
        
        playersTable = new TableView();
        playersTableBox = new VBox();
        playersTableBox.getChildren().add(playersTable);
        playerGridPane.add(playersTableBox, 0, 4);
        firstNameColumn = new TableColumn(COL_FIRSTNAME);
        lastNameColumn = new TableColumn(COL_LASTNAME);
        proTeamColumn = new TableColumn(COL_PROTEAM);
        positionsColumn = new TableColumn(COL_POSITIONS);
        yearOfBirthColumn = new TableColumn(COL_BIRTH);
        runWinsColumn = new TableColumn(COL_RUNWINS);
        homeRunsSavesColumn = new TableColumn(COL_HOMERUNS);
        rbiStrikeoutColumn = new TableColumn(COL_STRIKEOUTS);
        sbERAColumn = new TableColumn(COL_SBERA);
        baWHIPColumn = new TableColumn(COL_BAWHIP);
        notesColumn = new TableColumn(COL_NOTES);
        
        playersTable.getColumns().add(firstNameColumn);
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("firstName"));
        playersTable.getColumns().add(lastNameColumn);
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("lastName"));
        playersTable.getColumns().add(proTeamColumn);
        proTeamColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("proTeam"));
        playersTable.getColumns().add(positionsColumn);
        positionsColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("positions"));
        playersTable.getColumns().add(yearOfBirthColumn);
        yearOfBirthColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("birthYear"));
        playersTable.getColumns().add(runWinsColumn);
        runWinsColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("rw"));
        playersTable.getColumns().add(homeRunsSavesColumn);
        homeRunsSavesColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("hr"));
        playersTable.getColumns().add(rbiStrikeoutColumn);
        rbiStrikeoutColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("rbi"));
        playersTable.getColumns().add(sbERAColumn);
        sbERAColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("sb"));
        playersTable.getColumns().add(baWHIPColumn);
        baWHIPColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("ba"));
        playersTable.getColumns().add(notesColumn);
        notesColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("notes"));
        loadPlayers();
        dataManager.getCourse().setInitPlayers(players);
        playersTable.setItems(dataManager.getCourse().getPlayers());
        
        
        // AND NOW ADD THEM TO THE GRID
        playersPane.getChildren().add(playerGridPane);
        radioButtonsToolbar.getChildren().add(radioButtonsPane);
        playerGridPane.add(playerControlsToolbar, 0, 1);
        playerGridPane.add(searchPlayerLabel, 1, 1);
        playerGridPane.add(radioButtonsToolbar, 0, 3);
        
        fantasyGridPane = new GridPane();
        fantasyPane = new VBox();
        fantasyPane.getStyleClass().add(CLASS_BORDERED_PANE);
        fantasyPaneLabel = initChildLabel(fantasyPane, WDK_PropertyType.FANTASY_TEAMS_LABEL, CLASS_HEADING_LABEL);
        
        // PLAYER CONTROL BOX WITH ADD, REMOVE, AND SEARCH
        fantasyTeamControlsBox = new VBox();
        fantasyTeamControlsToolbar = new HBox();
        addFantasyTeamButton = initChildButton(fantasyTeamControlsToolbar, WDK_PropertyType.ADD_ICON, WDK_PropertyType.ADD_ITEM_TOOLTIP, false);
        removeFantasyTeamButton = initChildButton(fantasyTeamControlsToolbar, WDK_PropertyType.MINUS_ICON, WDK_PropertyType.REMOVE_ITEM_TOOLTIP, false);
        draftNameLabel = initGridLabel(fantasyGridPane, WDK_PropertyType.DRAFT_NAME_LABEL, CLASS_SUBHEADING_LABEL, 0, 1, 1, 1);
        draftNameTextfield = initGridTextField(fantasyGridPane, LARGE_TEXT_FIELD_LENGTH, EMPTY_TEXT, true, 1, 1, 1, 1);
        selectTeamLabel = initChildLabel(fantasyTeamControlsToolbar, WDK_PropertyType.SELECT_FANTASY_TEAM_LABEL, CLASS_SUBHEADING_LABEL);
        fantasyTeamComboBox = initGridComboBox(fantasyGridPane, 1, 2, 1, 1);
        fantasyTeamControlsBox.getStyleClass().add(CLASS_BORDERED_PANE);
        
        fantasyPane.getChildren().add(fantasyGridPane);
        fantasyGridPane.add(fantasyTeamControlsToolbar, 0, 2);
        
        fantasyTeamComboBox.setItems(dataManager.getCourse().getFantasyTeamStringNames());
        
        firstNameFColumn = new TableColumn(COL_FIRSTNAME);
        lastNameFColumn = new TableColumn(COL_LASTNAME);
        proTeamFColumn = new TableColumn(COL_PROTEAM);
        positionFColumn = new TableColumn(COL_POSITION);
        positionsFColumn = new TableColumn(COL_POSITIONS);
        yearOfBirthFColumn = new TableColumn(COL_BIRTH);
        runWinsFColumn = new TableColumn(COL_RUNWINS);
        homeRunsSavesFColumn = new TableColumn(COL_HOMERUNS);
        rbiStrikeoutFColumn = new TableColumn(COL_STRIKEOUTS);
        sbERAFColumn = new TableColumn(COL_SBERA);
        baWHIPFColumn = new TableColumn(COL_BAWHIP);
        valueFColumn = new TableColumn(COL_VALUE);
        salaryFColumn = new TableColumn(COL_SALARY);
        
        fantasyTeamsTable = new TableView();
        fantasyGridPane.add(fantasyTeamsTable, 0, 3);
        fantasyTeamsTable.getColumns().add(positionFColumn);
        positionFColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("position"));
        fantasyTeamsTable.getColumns().add(firstNameFColumn);
        firstNameFColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("firstName"));
        fantasyTeamsTable.getColumns().add(lastNameFColumn);
        lastNameFColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("lastName"));
        fantasyTeamsTable.getColumns().add(proTeamFColumn);
        proTeamFColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("proTeam"));
        fantasyTeamsTable.getColumns().add(yearOfBirthFColumn);
        yearOfBirthFColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("birthYear"));
        fantasyTeamsTable.getColumns().add(positionsFColumn);
        positionsFColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("positions"));
        fantasyTeamsTable.getColumns().add(runWinsFColumn);
        runWinsFColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("rw"));
        fantasyTeamsTable.getColumns().add(homeRunsSavesFColumn);
        homeRunsSavesFColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("hr"));
        fantasyTeamsTable.getColumns().add(rbiStrikeoutFColumn);
        rbiStrikeoutFColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("rbi"));
        fantasyTeamsTable.getColumns().add(sbERAFColumn);
        sbERAFColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("sb"));
        fantasyTeamsTable.getColumns().add(baWHIPFColumn);
        baWHIPFColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("ba"));
        fantasyTeamsTable.getColumns().add(valueFColumn);
        valueFColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("value"));
        fantasyTeamsTable.getColumns().add(salaryFColumn);
        salaryFColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("salary"));
        
        standingsPane = new VBox();
        standingsPane.getStyleClass().add(CLASS_BORDERED_PANE);
        standingsLabel = initChildLabel(standingsPane, WDK_PropertyType.STANDINGS_LABEL, CLASS_HEADING_LABEL);
        
        draftPane = new VBox();
        draftPane.getStyleClass().add(CLASS_BORDERED_PANE);
        draftLabel = initChildLabel(draftPane, WDK_PropertyType.DRAFT_LABEL, CLASS_HEADING_LABEL);
        
        proTeamsPane = new VBox();
        proTeamsPane.getStyleClass().add(CLASS_BORDERED_PANE);
        proTeamsLabel = initChildLabel(proTeamsPane, WDK_PropertyType.PROTEAMS_LABEL, CLASS_HEADING_LABEL);
    }

    // INITIALIZES THE CONTROLS IN THE LEFT HALF OF THE TOP WORKSPACE
    private void initBasicCourseInfoControls() throws IOException {
        // THESE ARE THE CONTROLS FOR THE BASIC SCHEDULE PAGE HEADER INFO
        // WE'LL ARRANGE THEM IN THE LEFT SIDE IN A VBox
//        courseInfoPane = new GridPane();
//
//        // FIRST THE HEADING LABEL
//        courseInfoLabel = initGridLabel(courseInfoPane, WDK_PropertyType.PLAYERS_HEADING_LABEL, CLASS_SUBHEADING_LABEL, 0, 0, 4, 1);
        // THEN CONTROLS FOR CHOOSING THE SUBJECT
//        courseSubjectLabel = initGridLabel(courseInfoPane, WDK_PropertyType.COURSE_SUBJECT_LABEL, CLASS_PROMPT_LABEL, 0, 1, 1, 1);
//        courseSubjectComboBox = initGridComboBox(courseInfoPane, 1, 1, 1, 1);
//        loadSubjectComboBox(subjects);
//
//        // THEN CONTROLS FOR UPDATING THE COURSE NUMBER
//        courseNumberLabel = initGridLabel(courseInfoPane, WDK_PropertyType.COURSE_NUMBER_LABEL, CLASS_PROMPT_LABEL, 2, 1, 1, 1);
//        courseNumberTextField = initGridTextField(courseInfoPane, SMALL_TEXT_FIELD_LENGTH, EMPTY_TEXT, true, 3, 1, 1, 1);
//
//        // THEN THE COURSE TITLE
//        courseTitleLabel = initGridLabel(courseInfoPane, WDK_PropertyType.COURSE_TITLE_LABEL, CLASS_PROMPT_LABEL, 0, 3, 1, 1);
//        courseTitleTextField = initGridTextField(courseInfoPane, LARGE_TEXT_FIELD_LENGTH, EMPTY_TEXT, true, 1, 3, 3, 1);
//
//        // THEN THE INSTRUCTOR NAME
//        instructorNameLabel = initGridLabel(courseInfoPane, WDK_PropertyType.INSTRUCTOR_NAME_LABEL, CLASS_PROMPT_LABEL, 0, 4, 1, 1);
//        instructorNameTextField = initGridTextField(courseInfoPane, LARGE_TEXT_FIELD_LENGTH, EMPTY_TEXT, true, 1, 4, 3, 1);
//
//        // AND THE INSTRUCTOR HOMEPAGE
//        instructorURLLabel = initGridLabel(courseInfoPane, WDK_PropertyType.INSTRUCTOR_URL_LABEL, CLASS_PROMPT_LABEL, 0, 5, 1, 1);
//        instructorURLTextField = initGridTextField(courseInfoPane, LARGE_TEXT_FIELD_LENGTH, EMPTY_TEXT, true, 1, 5, 3, 1);
    }

    // INITIALIZES THE CONTROLS IN THE RIGHT HALF OF THE TOP WORKSPACE
    private void initPageSelectionControls() {
        // THESE ARE THE CONTROLS FOR SELECTING WHICH PAGES THE SCHEDULE
        // PAGE WILL HAVE TO LINK TO
//        pagesSelectionPane = new VBox();
//        pagesSelectionPane.getStyleClass().add(CLASS_SUBJECT_PANE);
//        pagesSelectionLabel = initChildLabel(pagesSelectionPane, WDK_PropertyType.PAGES_SELECTION_HEADING_LABEL, CLASS_SUBHEADING_LABEL);
//        indexPageCheckBox = initChildCheckBox(pagesSelectionPane, CourseSiteExporter.INDEX_PAGE);
//        syllabusPageCheckBox = initChildCheckBox(pagesSelectionPane, CourseSiteExporter.SYLLABUS_PAGE);
//        schedulePageCheckBox = initChildCheckBox(pagesSelectionPane, CourseSiteExporter.SCHEDULE_PAGE);
//        hwsPageCheckBox = initChildCheckBox(pagesSelectionPane, CourseSiteExporter.HWS_PAGE);
//        projectsPageCheckBox = initChildCheckBox(pagesSelectionPane, CourseSiteExporter.PROJECTS_PAGE);
    }
    
    // INITIALIZE THE SCHEDULE ITEMS CONTROLS
    private void initScheduleItemsControls() {
        // FOR THE LEFT
//        dateBoundariesPane = new GridPane();
//        dateBoundariesLabel = initGridLabel(dateBoundariesPane, WDK_PropertyType.DATE_BOUNDARIES_LABEL, CLASS_SUBHEADING_LABEL, 0, 0, 1, 1);
//        startDateLabel = initGridLabel(dateBoundariesPane, WDK_PropertyType.STARTING_MONDAY_LABEL, CLASS_PROMPT_LABEL, 0, 1, 1, 1);
//        startDatePicker = initGridDatePicker(dateBoundariesPane, 1, 1, 1, 1);
//        endDateLabel = initGridLabel(dateBoundariesPane, WDK_PropertyType.ENDING_FRIDAY_LABEL, CLASS_PROMPT_LABEL, 0, 2, 1, 1);
//        endDatePicker = initGridDatePicker(dateBoundariesPane, 1, 2, 1, 1);
//
//        // THIS ONE IS ON THE RIGHT
//        lectureDaySelectorPane = new VBox();
//        lectureDaySelectLabel = initChildLabel(lectureDaySelectorPane, WDK_PropertyType.LECTURE_DAY_SELECT_LABEL, CLASS_SUBHEADING_LABEL);
//        mondayCheckBox = initChildCheckBox(lectureDaySelectorPane, CourseSiteExporter.MONDAY_HEADER);
//        tuesdayCheckBox = initChildCheckBox(lectureDaySelectorPane, CourseSiteExporter.TUESDAY_HEADER);
//        wednesdayCheckBox = initChildCheckBox(lectureDaySelectorPane, CourseSiteExporter.WEDNESDAY_HEADER);
//        thursdayCheckBox = initChildCheckBox(lectureDaySelectorPane, CourseSiteExporter.THURSDAY_HEADER);
//        fridayCheckBox = initChildCheckBox(lectureDaySelectorPane, CourseSiteExporter.FRIDAY_HEADER);
//
//        // THIS SPLITS THE TOP
//        splitScheduleInfoPane = new SplitPane();
//        splitScheduleInfoPane.getItems().add(dateBoundariesPane);
//        splitScheduleInfoPane.getItems().add(lectureDaySelectorPane);
//
//        // THIS IS FOR STUFF IN THE TOP OF THE SCHEDULE PANE, WE NEED TO PUT TWO THINGS INSIDE
//        scheduleInfoPane = new VBox();
//
//        // FIRST OUR SCHEDULE HEADER
//        scheduleInfoHeadingLabel = initChildLabel(scheduleInfoPane, WDK_PropertyType.SCHEDULE_HEADING_LABEL, CLASS_HEADING_LABEL);
//
//        // AND THEN THE SPLIT PANE
//        scheduleInfoPane.getChildren().add(splitScheduleInfoPane);
//
//        // FINALLY, EVERYTHING IN THIS REGION ULTIMATELY GOES INTO schedulePane
//        schedulePane = new VBox();
//        schedulePane.getChildren().add(scheduleInfoPane);
//        schedulePane.getStyleClass().add(CLASS_BORDERED_PANE);
    }

    // INITIALIZE THE WINDOW (i.e. STAGE) PUTTING ALL THE CONTROLS
    // THERE EXCEPT THE WORKSPACE, WHICH WILL BE ADDED THE FIRST
    // TIME A NEW Course IS CREATED OR LOADED
    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        primaryStage.setTitle(windowTitle);

        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A COURSE
        wdkPane = new BorderPane();
        wdkPane.setTop(fileToolbarPane);
        
        primaryScene = new Scene(wdkPane);

        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        primaryScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    // INIT ALL THE EVENT HANDLERS
    private void initEventHandlers() throws IOException {
        // FIRST THE FILE CONTROLS
        fileController = new FileController(primaryStage, courseFileManager, siteExporter);
        newCourseButton.setOnAction(e -> {
            fileController.handleNewCourseRequest(this);
        });
        saveCourseButton.setOnAction(e -> {
            fileController.handleSaveCourseRequest(this, dataManager.getCourse());
        });
        exportSiteButton.setOnAction(e -> {
            fileController.handleExportCourseRequest(this);
        });
        exitButton.setOnAction(e -> {
            fileController.handleExitRequest(this);
        });
        
        fantasyTeamsButton.setOnAction(e -> {
            workspacePane.setCenter(fantasyPane);
        });
        playersButton.setOnAction(e -> {
            workspacePane.setCenter(playersPane);
        });
        standingsButton.setOnAction(e -> {
            workspacePane.setCenter(standingsPane);
        });
        draftButton.setOnAction(e -> {
            workspacePane.setCenter(draftPane);
        });
        proTeamsButton.setOnAction(e -> {
            workspacePane.setCenter(proTeamsPane);
        });
        
        playerController = new PlayerController(primaryStage, messageDialog, yesNoCancelDialog);
        addPlayerButton.setOnAction(e -> {
            playerController.handleAddPlayerRequest(this);
        });
        removePlayerButton.setOnAction(e -> {
            playerController.handleRemovePlayerRequest(this, playersTable.getSelectionModel().getSelectedItem());
        });
        playersTable.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                // OPEN UP THE SCHEDULE ITEM EDITOR
                Player p = playersTable.getSelectionModel().getSelectedItem();
                playerController.handleEditPlayerRequest(this, p);
            }
        });
        addFantasyTeamButton.setOnAction(e -> {
            playerController.handleAddFantasyTeamRequest(this);
        });
        removeFantasyTeamButton.setOnAction(e -> {
            playerController.handleRemoveFantasyTeamRequest(this, fantasyTeamComboBox.getSelectionModel().getSelectedItem().toString());
        });
//        removeFantasyTeamButton.setOnAction(e -> {
//            playerController.handleRemoveFantasyTeamRequest(this, playersTable.getSelectionModel().getSelectedItem());
//        });
        // THEN THE COURSE EDITING CONTROLS
//        courseController = new CourseEditController();
//        courseSubjectComboBox.setOnAction(e -> {
//            courseController.handleCourseChangeRequest(this);
//        });
//        indexPageCheckBox.setOnAction(e -> {
//            courseController.handleCourseChangeRequest(this);
//        });
//        syllabusPageCheckBox.setOnAction(e -> {
//            courseController.handleCourseChangeRequest(this);
//        });
//        schedulePageCheckBox.setOnAction(e -> {
//            courseController.handleCourseChangeRequest(this);
//        });
//        hwsPageCheckBox.setOnAction(e -> {
//            courseController.handleCourseChangeRequest(this);
//        });
//        projectsPageCheckBox.setOnAction(e -> {
//            courseController.handleCourseChangeRequest(this);
//        });

        // TEXT FIELDS HAVE A DIFFERENT WAY OF LISTENING FOR TEXT CHANGES
//        registerTextFieldController(courseNumberTextField);
//        registerTextFieldController(courseTitleTextField);
//        registerTextFieldController(instructorNameTextField);
//        registerTextFieldController(instructorURLTextField);

//        // THE DATE SELECTION ONES HAVE PARTICULAR CONCERNS, AND SO
//        // GO THROUGH A DIFFERENT METHOD
//        startDatePicker.setOnAction(e -> {
//            courseController.handleDateSelectionRequest(this, startDatePicker, endDatePicker);
//        });
//        endDatePicker.setOnAction(e -> {
//            courseController.handleDateSelectionRequest(this, startDatePicker, endDatePicker);
//        });
//
//        // AND THE LECTURE DAYS CHECKBOXES
//        mondayCheckBox.setOnAction(e -> {
//            courseController.handleCourseChangeRequest(this);
//        });
//        tuesdayCheckBox.setOnAction(e -> {
//            courseController.handleCourseChangeRequest(this);
//        });
//        wednesdayCheckBox.setOnAction(e -> {
//            courseController.handleCourseChangeRequest(this);
//        });
//        thursdayCheckBox.setOnAction(e -> {
//            courseController.handleCourseChangeRequest(this);
//        });
//        fridayCheckBox.setOnAction(e -> {
//            courseController.handleCourseChangeRequest(this);
//        });
    }

    // REGISTER THE EVENT LISTENER FOR A TEXT FIELD
    private void registerTextFieldController(TextField textField) {
//        textField.textProperty().addListener((observable, oldValue, newValue) -> {
//            courseController.handleCourseChangeRequest(this);
//        });
    }
    // INIT A BUTTON AND ADD IT TO A CONTAINER IN A TOOLBAR
    private Button initChildButton(Pane toolbar, WDK_PropertyType icon, WDK_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
    
    // INIT A LABEL AND SET IT'S STYLESHEET CLASS
    private Label initLabel(WDK_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }

    // INIT A LABEL AND PLACE IT IN A GridPane INIT ITS PROPER PLACE
    private Label initGridLabel(GridPane container, WDK_PropertyType labelProperty, String styleClass, int col, int row, int colSpan, int rowSpan) {
        Label label = initLabel(labelProperty, styleClass);
        container.add(label, col, row, colSpan, rowSpan);
        return label;
    }

    // INIT A LABEL AND PUT IT IN A TOOLBAR
    private Label initChildLabel(Pane container, WDK_PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }

    // INIT A COMBO BOX AND PUT IT IN A GridPane
    private ComboBox initGridComboBox(GridPane container, int col, int row, int colSpan, int rowSpan) throws IOException {
        ComboBox comboBox = new ComboBox();
        container.add(comboBox, col, row, colSpan, rowSpan);
        return comboBox;
    }

    // LOAD THE COMBO BOX TO HOLD Course SUBJECTS
    private void loadSubjectComboBox(ArrayList<String> subjects) {
//        for (String s : subjects) {
//            courseSubjectComboBox.getItems().add(s);
//        }
    }

    // INIT A TEXT FIELD AND PUT IT IN A GridPane
    private TextField initGridTextField(GridPane container, int size, String initText, boolean editable, int col, int row, int colSpan, int rowSpan) {
        TextField tf = new TextField();
        tf.setPrefColumnCount(size);
        tf.setText(initText);
        tf.setEditable(editable);
        container.add(tf, col, row, colSpan, rowSpan);
        return tf;
    }

    // INIT A DatePicker AND PUT IT IN A GridPane
    private DatePicker initGridDatePicker(GridPane container, int col, int row, int colSpan, int rowSpan) {
        DatePicker datePicker = new DatePicker();
        container.add(datePicker, col, row, colSpan, rowSpan);
        return datePicker;
    }

    // INIT A CheckBox AND PUT IT IN A TOOLBAR
    private CheckBox initChildCheckBox(Pane container, String text) {
        CheckBox cB = new CheckBox(text);
        container.getChildren().add(cB);
        return cB;
    }

    // INIT A DatePicker AND PUT IT IN A CONTAINER
    private DatePicker initChildDatePicker(Pane container) {
        DatePicker dp = new DatePicker();
        container.getChildren().add(dp);
        return dp;
    }
    
    // LOADS CHECKBOX DATA INTO A Course OBJECT REPRESENTING A CoursePage
    private void updatePageUsingCheckBox(CheckBox cB, Course course, CoursePage cP) {
        if (cB.isSelected()) {
            course.selectPage(cP);
        } else {
            course.unselectPage(cP);
        }
    }    
    
    private RadioButton initRadioButton(GridPane container, String text, int col, int row, int colSpan, int rowSpan){
        RadioButton rB = new RadioButton(text);
        container.add(rB, col, row, colSpan, rowSpan);
        return rB;
    }
    
    public void loadPlayers() throws IOException{
        File hittersFile = new File("./data/Hitters.json");
        if (hittersFile != null) {
        JsonObject json = loadJSONFile(hittersFile.getAbsolutePath());
        JsonArray jsonHittersArray = json.getJsonArray(JSON_HITTERS);
        players.clear();
        for (int i = 0; i < jsonHittersArray.size(); i++) {
            JsonObject jso = jsonHittersArray.getJsonObject(i);
            Player p = new Player();
            p.setFirstName(jso.getString(JSON_FIRST_NAME));
            p.setLastName(jso.getString(JSON_LAST_NAME));
            p.setProTeam(jso.getString(JSON_TEAM));
            p.setPositions(jso.getString(JSON_POSITION));
            p.setBirthYear(jso.getString(JSON_YEAR_OF_BIRTH));
            p.setRW(jso.getString(JSON_R));
            p.setHR(jso.getString(JSON_HR));
            p.setRBI(jso.getString(JSON_RBI));
            p.setSB(jso.getString(JSON_SB));
            double ba = (Double.parseDouble(jso.getString(JSON_H)))/(Double.parseDouble(jso.getString(JSON_AB)));
            p.setBA(String.format("%.3f",ba));
            p.setNotes(jso.getString(JSON_NOTES));
            // ADD IT TO TABLE
            players.add(p);
        }
        //data.clear();
        File pitchersFile = new File("./data/Pitchers.json");
        if (pitchersFile != null) {
        JsonObject json2 = loadJSONFile(pitchersFile.getAbsolutePath());
        JsonArray jsonPitchersArray = json2.getJsonArray(JSON_PITCHERS);
        for (int i = 0; i < jsonPitchersArray.size(); i++) {
            JsonObject jso = jsonPitchersArray.getJsonObject(i);
            Player p = new Player();
            p.setFirstName(jso.getString(JSON_FIRST_NAME));
            p.setLastName(jso.getString(JSON_LAST_NAME));
            p.setProTeam(jso.getString(JSON_TEAM));
            p.setPositions("P");
            p.setBirthYear(jso.getString(JSON_YEAR_OF_BIRTH));
            p.setRW(jso.getString(JSON_W));
            p.setHR(jso.getString(JSON_SV));
            p.setRBI(jso.getString(JSON_K));
            double era = (Double.parseDouble(jso.getString(JSON_ER))*9)/(Double.parseDouble(jso.getString(JSON_IP)));
            p.setSB(String.format("%.2f",era));
            double whip = ((Double.parseDouble(jso.getString(JSON_BB)))+(Double.parseDouble(jso.getString(JSON_H))))/(Double.parseDouble(jso.getString(JSON_IP)));
            p.setBA(String.format("%.2f",whip));
            p.setNotes(jso.getString(JSON_NOTES));
            // ADD IT TO TABLE
            players.add(p);
            playersTable.setItems(players);
        }
        }
        }
    }
        // LOADS A JSON FILE AS A SINGLE OBJECT AND RETURNS IT
        private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }    
    
    // LOADS AN ARRAY OF A SPECIFIC NAME FROM A JSON FILE AND
    // RETURNS IT AS AN ArrayList FULL OF THE DATA FOUND
    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ArrayList<String> items = new ArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (JsonValue jsV : jsonArray) {
            items.add(jsV.toString());
        }
        return items;
    }
}
