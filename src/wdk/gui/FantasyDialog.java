package wdk.gui;
import wdk.data.Player;
import static wdk.gui.WDK_GUI.CLASS_HEADING_LABEL;
import static wdk.gui.WDK_GUI.CLASS_PROMPT_LABEL;
import static wdk.gui.WDK_GUI.PRIMARY_STYLE_SHEET;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wdk.data.FantasyTeam;
import static wdk.gui.PlayerDialog.FIRST_NAME_PROMPT;
/**
 *
 * @author Victor
 */
public class FantasyDialog extends Stage{
    FantasyTeam fantasyTeam;
    
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label fantasyTeamLabel;
    Label fantasyOwnerLabel;
    TextField fantasyTeamTextField;
    TextField fantasyOwnerTextField;   
    Button completeButton;
    Button cancelButton;
    
    String selection;
    
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String FANTASY_NAME_PROMPT = "Name: ";
    public static final String OWNER_PROMPT = "Owner: ";
    public static final String FANTASY_HEADING = "Fantasy Team Details";
    public static final String ADD_FANTASY_TEAM_TITLE = "Add New Fantasy Team";
    public static final String EDIT_FANTASY_TEAM_TITLE = "Edit Fantasy Team";
    
    public FantasyDialog(Stage primaryStage) {
        // FIRST MAKE OUR PLAYER AND INITIALIZE
        // IT WITH DEFAULT VALUES
        fantasyTeam = new FantasyTeam();
        
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(FANTASY_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        // NOW THE FIRST_NAME 
//        firstNameLabel = new Label(FIRST_NAME_PROMPT);
//        firstNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
//        firstNameTextField = new TextField();
//        firstNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
//            player.setFirstName(newValue);
//        });
        
        // NOW THE LAST_NAME 
        headingLabel = new Label(FANTASY_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        
        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            FantasyDialog.this.selection = sourceButton.getText();
            FantasyDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel,      0, 0, 2, 1);
        gridPane.add(completeButton,    0, 6, 1, 1);
        gridPane.add(cancelButton,      1, 6, 1, 1);

        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
//        fantasyTeam = new FantasyTeam();
//        
//        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
//        // FOR IT WHEN IT IS DISPLAYED
//        initModality(Modality.WINDOW_MODAL);
//        initOwner(primaryStage);
//        
//        // FIRST OUR CONTAINER
//        gridPane = new GridPane();
//        gridPane.setPadding(new Insets(10, 20, 20, 20));
//        gridPane.setHgap(10);
//        gridPane.setVgap(10);
//        
//        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
//        // ON WHETHER WE'RE ADDING OR EDITING
//        headingLabel = new Label(FANTASY_HEADING);
//        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
//        
//        // NOW THE FIRST_NAME 
//        fantasyTeamLabel = new Label(FANTASY_NAME_PROMPT);
//        fantasyTeamLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
//        fantasyTeamTextField = new TextField();
//        fantasyTeamTextField.textProperty().addListener((observable, oldValue, newValue) -> {
//            fantasyTeam.setDraftName(newValue);
//        });
//        
//        fantasyOwnerLabel = new Label(FANTASY_NAME_PROMPT);
//        fantasyOwnerLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
//        fantasyOwnerTextField = new TextField();
//        fantasyOwnerTextField.textProperty().addListener((observable, oldValue, newValue) -> {
//            fantasyTeam.setFantasyOwner(newValue);
//        });
//        
//        completeButton = new Button(COMPLETE);
//        cancelButton = new Button(CANCEL);
//        
//        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
//        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
//            Button sourceButton = (Button)ae.getSource();
//            FantasyDialog.this.selection = sourceButton.getText();
//            FantasyDialog.this.hide();
//        };
//        completeButton.setOnAction(completeCancelHandler);
//        cancelButton.setOnAction(completeCancelHandler);
//        
//        gridPane.add(headingLabel,      0, 0, 2, 1);
//        gridPane.add(completeButton,    0, 6, 1, 1);
//        gridPane.add(cancelButton,      1, 6, 1, 1);
//          
//        dialogScene = new Scene(gridPane);
//        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
//        this.setScene(dialogScene);
    }
    
    public String getSelection() {
        return selection;
    }
    
    public FantasyTeam getFantasyTeam() { 
        return fantasyTeam;
    }
    
    public FantasyTeam showAddFantasyTeamDialog() {
        // SET THE DIALOG TITLE
        setTitle(ADD_FANTASY_TEAM_TITLE);
        
        // RESET THE SCHEDULE ITEM OBJECT WITH DEFAULT VALUES
        fantasyTeam = new FantasyTeam();
        
        // LOAD THE UI STUFF
        fantasyTeamTextField.setText(fantasyTeam.getDraftName());
        fantasyOwnerTextField.setText(fantasyTeam.getFantasyOwner());
        //proTeamsComboBox.getSelectionModel().select(0);
        
        // AND OPEN IT UP
        this.showAndWait();
        
        return fantasyTeam;
    }
    
    public void loadGUIData() {
        // LOAD THE UI STUFF
//        firstNameTextField.setText(player.getFirstName());
//        lastNameTextField.setText(player.getLastName());
        //int proTeamsIndex = player.getProTeam()-1;
        //proTeamsComboBox.getSelectionModel().select(positionsIndex);
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    public void showEditFantasyTeamDialog(FantasyTeam teamToEdit) {
        // SET THE DIALOG TITLE
        setTitle(EDIT_FANTASY_TEAM_TITLE);
        
        // LOAD THE PLAYER INTO OUR LOCAL OBJECT
        

        // AND THEN INTO OUR GUI
        loadGUIData();
               
        // AND OPEN IT UP
        this.showAndWait();
    }
}
