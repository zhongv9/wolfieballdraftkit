package wdk.controller;

import static wdk.WDK_PropertyType.REMOVE_ITEM_MESSAGE;
import wdk.data.Course;
import wdk.data.CourseDataManager;
import wdk.data.Player;
import wdk.gui.WDK_GUI;
import wdk.gui.PlayerDialog;
import wdk.gui.MessageDialog;
import wdk.gui.YesNoCancelDialog;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wdk.data.FantasyTeam;
import wdk.gui.FantasyDialog;

/**
 *
 * @author McKillaGorilla
 */
public class PlayerController {
    PlayerDialog pd, epd, fd;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    public PlayerController(Stage initPrimaryStage, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        pd = new PlayerDialog(initPrimaryStage);
        epd = new PlayerDialog(initPrimaryStage, "");
        fd = new PlayerDialog(initPrimaryStage, "", "");
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }

    public void handleAddPlayerRequest(WDK_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        pd.showAddPlayerDialog();
        
        // DID THE USER CONFIRM?
        if (pd.wasCompleteSelected()) {
            // GET THE SCHEDULE ITEM
            Player l = pd.getPlayer();
            
            // AND ADD IT AS A ROW TO THE TABLE
            Player newPlayer = new Player();
            newPlayer.setFirstName(l.getFirstName());
            newPlayer.setLastName(l.getLastName());
            newPlayer.setProTeam(l.getProTeam());
            newPlayer.setPositions(l.getPositions());
            course.addPlayer(newPlayer);
            
            // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
            // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
            // THE SAVE BUTTON IS ENABLED
            gui.getFileController().markAsEdited(gui);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
        
    }
    
    public void handleRemovePlayerRequest(WDK_GUI gui, Player playerToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
//        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
//        
//        // AND NOW GET THE USER'S SELECTION
//        String selection = yesNoCancelDialog.getSelection();
//
//        // IF THE USER SAID YES, THEN REMOVE IT
//        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removePlayer(playerToRemove);
            
            // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
            // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
            // THE SAVE BUTTON IS ENABLED
//            gui.getFileController().markAsEdited(gui);
//        }
    }
    
    public void handleEditPlayerRequest(WDK_GUI gui, Player playerToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        epd.showEditPlayerDialog(playerToEdit);
        
        // DID THE USER CONFIRM?
        if (epd.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            Player l = epd.getPlayer();
            playerToEdit.setFirstName(l.getFirstName());
            playerToEdit.setLastName(l.getLastName());
            
            // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
            // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
            // THE SAVE BUTTON IS ENABLED
            gui.getFileController().markAsEdited(gui);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }              
    }
    
    public void handleAddFantasyTeamRequest(WDK_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        fd.showAddFantasyTeamDialog();
        
        // DID THE USER CONFIRM?
        if (fd.wasCompleteSelected()) {
            // GET THE SCHEDULE ITEM
            FantasyTeam f = fd.getFantasyTeam();
            
            FantasyTeam newTeam = new FantasyTeam();
            newTeam.setTeamName(f.getTeamName());
            newTeam.setFantasyOwner(f.getFantasyOwner());
            course.addFantasyTeamStringName(f.getTeamName());
            epd.addFantasyTeamStringName(f.getTeamName());
            
            // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
            // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
            // THE SAVE BUTTON IS ENABLED
            gui.getFileController().markAsEdited(gui);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    
     public void handleRemoveFantasyTeamRequest(WDK_GUI gui, String itemToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
//        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
//        
//        // AND NOW GET THE USER'S SELECTION
//        String selection = yesNoCancelDialog.getSelection();
//
//        // IF THE USER SAID YES, THEN REMOVE IT
//        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeFantasyTeamStringName(itemToRemove);
            
            // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
            // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
            // THE SAVE BUTTON IS ENABLED
//            gui.getFileController().markAsEdited(gui);
//        }
    }
}