package wdk.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Player {
    final StringProperty firstName, lastName, proTeam, fantasyTeam, position, positions, birthYear, contract, salary, notes, rw, hr, rbi, sb, ba;
    //final IntegerProperty rw, hr, rbi, sb, ba;
    //String firstName, String lastName, String proTeam, String position, String birthYear, int rw, int hr, int rbi, int sb, int ba, String notes
    public Player() {
        this.firstName = new SimpleStringProperty();
        this.lastName = new SimpleStringProperty();
        this.proTeam = new SimpleStringProperty();
        this.fantasyTeam = new SimpleStringProperty();
        this.position = new SimpleStringProperty();
        this.positions = new SimpleStringProperty();
        this.birthYear = new SimpleStringProperty();
        this.rw = new SimpleStringProperty();
        this.hr = new SimpleStringProperty();
        this.rbi = new SimpleStringProperty();
        this.sb = new SimpleStringProperty();
        this.ba = new SimpleStringProperty();
        this.notes = new SimpleStringProperty();
        this.contract = new SimpleStringProperty();
        this.salary = new SimpleStringProperty();
    }

    public String getFirstName() 
    {
        return firstName.get();
    }
    
    public void setFirstName(String initFirstName){
        firstName.set(initFirstName);
    }
    
    public StringProperty firstNameProperty() {
        return firstName;
    }
    
    public String getLastName() 
    {
        return lastName.get();
    }
    
    public void setLastName(String initLastName){
        lastName.set(initLastName);
    }
    
    public StringProperty lastNameProperty() {
        return lastName;
    }
    
    public String getProTeam() {
        return proTeam.get();
    }
    
    public void setProTeam(String initProTeam) {
        proTeam.set(initProTeam);
    }
    
    public StringProperty proTeamProperty() {
        return proTeam;
    }
    
    public String getFantasyTeam() {
        return fantasyTeam.get();
    }
    
    public void setFantasyTeam(String initFantasyTeam) {
        fantasyTeam.set(initFantasyTeam);
    }
    
    public StringProperty fantasyTeamProperty() {
        return fantasyTeam;
    }
    
    public String getPosition() {
        return position.get();
    }
    
    public void setPosition(String initPosition) {
        position.set(initPosition);
    }
    
    public StringProperty positionProperty() {
        return position;
    }
    
    public String getPositions() {
        return positions.get();
    }
    
    public void setPositions(String initPositions) {
        positions.set(initPositions);
    }
    
    public StringProperty positionsProperty() {
        return positions;
    }
    
    public String getBirthYear() {
        return birthYear.get();
    }
    
    public void setBirthYear(String initBirthYear) {
        birthYear.set(initBirthYear);
    }
    
    public StringProperty birthYearProperty() {
        return birthYear;
    }
    
    public String getRW() {
        return rw.get();
    }
    
    public void setRW(String newRW) {
        rw.set(newRW);
    }
    
    public StringProperty rwProperty() {
        return rw;
    }
    
    public String getHR() {
        return hr.get();
    }
    
    public void setHR(String newHR) {
        hr.set(newHR);
    }
    
    public StringProperty hrProperty() {
        return hr;
    }
    
    public String getRBI() {
        return rbi.get();
    }
    
    public void setRBI(String newRBI) {
        rbi.set(newRBI);
    }
    
    public StringProperty rbiProperty() {
        return rbi;
    }
    
    public String getSB() {
        return sb.get();
    }
    
    public void setSB(String newSB) {
        sb.set(newSB);
    }
    
    public StringProperty sbProperty() {
        return sb;
    }
    
    public String getBA() {
        return ba.get();
    }
    
    public void setBA(String newBA) {
        ba.set(newBA);
    }
    
    public StringProperty baProperty() {
        return ba;
    }
    
    public String getNotes() {
        return notes.get();
    }
    
    public void setNotes(String newNotes) {
        notes.set(newNotes);
    }
    
    public StringProperty notesProperty() {
        return notes;
    }
    
    public String getContract() {
        return contract.get();
    }
    
    public void setContract(String newContract) {
        contract.set(newContract);
    }
    
    public StringProperty contractProperty(){
        return contract;
    }
    
    public String getSalary() {
        return salary.get();
    }
    
    public void setSalary(String newSalary) {
        salary.set(newSalary);
    }
    
    public StringProperty salaryProperty(){
        return salary;
    }
//    public int compareTo(Object obj) {
//        Player otherPlayer = (Player)obj;
//        return getLastName().compareTo(otherPlayer.getLastName());
//    }
}