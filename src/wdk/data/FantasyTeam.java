package wdk.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
/**
 *
 * @author Victor
 */
public class FantasyTeam {
    final StringProperty teamName, fantasyTeamName, fantasyOwner;
    public FantasyTeam(){
        this.teamName = new SimpleStringProperty();
        this.fantasyTeamName = new SimpleStringProperty();
        this.fantasyOwner = new SimpleStringProperty();
    }
    
    public String getTeamName() 
    {
        return teamName.get();
    }
    
    public void setTeamName(String initDraftName){
        teamName.set(initDraftName);
    }
    
    public StringProperty teamNameProperty() {
        return teamName;
    }
    
    public String getFantasyTeamName() 
    {
        return fantasyTeamName.get();
    }
    
    public void setFantasyTeamName(String initFantasyTeamName){
        fantasyTeamName.set(initFantasyTeamName);
    }
    
    public StringProperty fantasyTeamNameProperty() {
        return fantasyTeamName;
    }
    
    public String getFantasyOwner() 
    {
        return fantasyOwner.get();
    }
    
    public void setFantasyOwner(String initFantasyOwner){
        fantasyOwner.set(initFantasyOwner);
    }
    
    public StringProperty fantasyOwnerProperty() {
        return fantasyOwner;
    }
}
